from flask import Flask, redirect, url_for, request, render_template

app = Flask(__name__)


@app.route('/admin')
def hello_admin():
    return "hello admin"


@app.route('/guest/<guest>')
def hello_guest(guest):
    return render_template('hello.html', name=guest)

# variable rules


@app.route('/')
def home():
    return render_template("home.html")


# variable rules
@app.route('/user/<name>', methods=['POST'])
def hello_user(name):
    if name == 'admin':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_guest', guest=name))


@app.route('/about/<int:pg_no>')
def page_number(pg_no):
    return f"Page no. : {pg_no}"


@app.route('/about/<float:version_no>')
def version_number(version_no):
    return f"Version no. : {version_no}"


@app.route('/login', methods=['POST','GET'])
def login():
    if request.method == 'POST':
        user = request.form['name_field']
        return redirect(url_for('hello_guest',guest =user))
    else:
        user = request.args.get('name_field')
        return redirect(url_for('hello_guest', guest=user))


@app.route('/index')
def index():
    return render_template("login.html")


if __name__ == "__main__":
    app.run(debug=True)
